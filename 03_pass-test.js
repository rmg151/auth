'use strict'

const bcrypt = require('bcrypt');

const passService = require('./services/pass.service');
const moment = require('moment');

//Datos simulados
const miPass = "1234"
const badPass = "6789";
const usuario = {
    _id: "12345678",
    email: 'rmg@ua.es',
    displayName: 'rmg',
    password: miPass,
    signupDate: moment().unix(),
    lastLogin: moment().unix()
};

console.log(usuario);

//encriptamos el pass
passService.encriptaPassword(usuario.password)
    .then(hash => {
        usuario.password = hash;
        console.log(usuario);

        //verificamos el pass
        passService.comparaPassword(miPass, usuario.password)
            .then (isOk => {
                if (isOk) {
                    console.log('p1: El pass es correcto');
                } else {
                    console.log('p1: El pass es incorrecto');
                }
            });
 
        //verificamos el pass con uno erroneo
        passService.comparaPassword(badPass, usuario.password)
        .then (isOk => {
            if (isOk) {
                console.log('p2: El pass es correcto');
            } else {
                console.log('p2: El pass es incorrecto');
            }
        });
    });





