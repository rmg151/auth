'use strict'

const tokenService = require('./services/token.service');
const moment = require('moment');

//simular datos
const miPass = "1234"
const usuario = {
    _id: "12345678",
    email: 'rmg@ua.es',
    displayName: 'rmg',
    password: miPass,
    signupDate: moment().unix(),
    lastLogin: moment().unix()
};

console.log(usuario);

//Crear un token
const token = tokenService.creaToken(usuario);
console.log(token);

//Decodificar token
tokenService.decodificaToken(token)
    .then(userID => {
        return console.log(`ID1: ${userID}`)
    })
    .catch(err => console.log({err1: err})
    );


//eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxMjM0NTY3OCIsImlhdCI6MTYwNjY3NDE1OSwiZXhwIjoxNjA3Mjc4OTU5fQ.qQVeuP8PWH7jZO75r7ZA7TQpdtQImupuP0aBF0cPvvk
